package me.hataProduction.feeCalculator;

import me.hataProduction.feeCalculator.brokers.bks.BksBroker;
import me.hataProduction.feeCalculator.brokers.bks.BksBrokerDescriptions;
import me.hataProduction.feeCalculator.brokers.finam.FinamBrokerDescriptions;
import me.hataProduction.feeCalculator.brokers.finam.FinamBrokerTaxes;
import me.hataProduction.feeCalculator.brokers.open.OpenBroker;
import me.hataProduction.feeCalculator.brokers.open.OpenBrokerDescriptions;
import me.hataProduction.feeCalculator.brokers.tinkoff.TinkoffBroker;
import me.hataProduction.feeCalculator.brokers.tinkoff.TinkoffBrokerDescriptions;

public class App {

    public static void main(String[] args) {
        Client client = new Client(Integer.parseInt(args[0]), Integer.parseInt(args[1]), Integer.parseInt(args[2]));
        int numPeriods = Integer.parseInt(args[3]);

        TaxesCalculator openBrokerCalc = new TaxesCalculator(client, new OpenBroker(), numPeriods);
        BrokerFeeConsolePrinter openBrokerConsolePrinter =
                new BrokerFeeConsolePrinter(client, openBrokerCalc, new OpenBrokerDescriptions());
        openBrokerConsolePrinter.print();

        TaxesCalculator bksBrokerCalc = new TaxesCalculator(client, new BksBroker(), numPeriods);
        BrokerFeeConsolePrinter bksBrokerConsolePrinter =
                new BrokerFeeConsolePrinter(client, bksBrokerCalc, new BksBrokerDescriptions());
        bksBrokerConsolePrinter.print();

        TaxesCalculator finamBrokerCalc = new TaxesCalculator(client, new FinamBrokerTaxes(), numPeriods);
        BrokerFeeConsolePrinter finamBrokerConsolePrinter =
                new BrokerFeeConsolePrinter(client, finamBrokerCalc, new FinamBrokerDescriptions());
        finamBrokerConsolePrinter.print();

        TaxesCalculator tinkoffBrokerCalc = new TaxesCalculator(client, new TinkoffBroker(), numPeriods);
        BrokerFeeConsolePrinter tinkoffPrinter = new BrokerFeeConsolePrinter(client, tinkoffBrokerCalc, new TinkoffBrokerDescriptions());
        tinkoffPrinter.print();
    }
}
