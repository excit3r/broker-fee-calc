package me.hataProduction.feeCalculator;

class Client {

    private int initialCapital;
    private int monthlyAddition;
    private int monthlyDealsNum;

    Client(int initialCapital, int monthlyAddition, int monthlyDealsNum) {
        this.initialCapital = initialCapital;
        this.monthlyAddition = monthlyAddition;
        this.monthlyDealsNum = monthlyDealsNum;
    }

    int getInitialCapital() {
        return initialCapital;
    }

    int getMonthlyAddition() {
        return monthlyAddition;
    }

    int getMonthlyDealsNum() {
        return monthlyDealsNum;
    }
}
