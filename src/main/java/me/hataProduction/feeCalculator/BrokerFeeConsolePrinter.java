package me.hataProduction.feeCalculator;

import me.hataProduction.feeCalculator.brokers.HasTaxerDescriptions;

import java.text.DecimalFormat;
import java.util.function.Function;
import java.util.stream.Collectors;

class BrokerFeeConsolePrinter {

    private Client client;
    private TaxesCalculator taxesCalculator;
    private HasTaxerDescriptions taxerDescriptions;
    private String hr = "_".repeat(150);

    private char tabular = '\t';
    private Function<Double, String> decimalFormatter = d -> new DecimalFormat("##.00").format(d);
    private Function<Object, String> tabularAppender = s -> s.toString() + tabular;

    BrokerFeeConsolePrinter(Client client,
                                   TaxesCalculator taxesCalculator,
                                   HasTaxerDescriptions taxerDescriptions) {
        this.client = client;
        this.taxesCalculator = taxesCalculator;
        this.taxerDescriptions = taxerDescriptions;
    }

    void print() {
        System.out.println(hr);
        System.out.println("Брокер " + taxerDescriptions.getName());
        System.out.println("Текущий капитал");
        System.out.println(taxesCalculator.getSums().stream().map(tabularAppender).collect(Collectors.joining()));
        System.out.println("Комиссия за оборот по сделкам в месяц (дополнительное вложение для новых сделок: " + client.getMonthlyAddition() + ") " + taxerDescriptions.getTradeTaxDescription());
        System.out.println(taxesCalculator.getTradeTaxes().stream().map(decimalFormatter).map(tabularAppender).collect(Collectors.joining()));
        System.out.println("Комиссия за оборот по сделкам в месяц (ежемесячная перетасовка портфеля)");
        System.out.println(taxesCalculator.getRebalancedTaxes().stream().map(decimalFormatter).map(tabularAppender).collect(Collectors.joining()));
        System.out.println("Комиссия за депозитарное хранение " + taxerDescriptions.getDepoTaxDescription());
        System.out.println(taxesCalculator.getDepoTaxes().stream().map(decimalFormatter).map(tabularAppender).collect(Collectors.joining()));
        System.out.println("Всего комиссий в месяц");
        System.out.println(taxesCalculator.getTotalMonthlyTaxes().stream().map(decimalFormatter).map(tabularAppender).collect(Collectors.joining()));
        System.out.println("Всего комиссий в месяц при перетасовке портфеля");
        System.out.println(taxesCalculator.getRebalancedSumTaxes().stream().map(decimalFormatter).map(tabularAppender).collect(Collectors.joining()));
        System.out.println("Итого комиссий за " + taxesCalculator.getNumPeriods() + " мес.: " + decimalFormatter.apply(taxesCalculator.getTotalPeriodTaxes()) + "р.");
        System.out.println("Итого комиссий при ежемесячной перетасовке портфеля за " + taxesCalculator.getNumPeriods() + " мес.: " + decimalFormatter.apply(taxesCalculator.getTotalPeriodRebalancedTaxes()) + "р.");
        System.out.println(hr);
    }
}
