package me.hataProduction.feeCalculator;

import me.hataProduction.feeCalculator.brokers.Taxer;

import java.util.ArrayList;
import java.util.List;

class TaxesCalculator {

    private Client client;
    private Taxer taxer;
    private int numPeriods;
    private List<Integer> sums = new ArrayList<>(numPeriods);
    private List<Double> tradeTaxes = new ArrayList<>(numPeriods);
    // Если каждый месяц перетасовываем весь портфель
    private List<Double> rebalancedTaxes = new ArrayList<>(numPeriods);
    private List<Double> rebalancedSumTaxes = new ArrayList<>(numPeriods);
    private List<Double> depoTaxes = new ArrayList<>(numPeriods);
    private List<Double> totalMonthlyTaxes = new ArrayList<>(numPeriods);
    private double totalPeriodRebalancedTaxes = 0.0;
    private double totalPeriodTaxes = 0.0;

    TaxesCalculator(Client client, Taxer taxer, int numPeriods) {
        this.client = client;
        this.taxer = taxer;
        this.numPeriods = numPeriods;
        initSumsRanges();
        initTaxes();
    }

    private void initSumsRanges() {
        this.sums = new ArrayList<>(numPeriods);
        int currentCapital = client.getInitialCapital();
        for (int i = 0; i < numPeriods; i++) {
            if (i == 0) {
                sums.add(currentCapital);
            } else {
                currentCapital += client.getMonthlyAddition();
                sums.add(currentCapital);
            }
        }
    }

    private void initTaxes() {
        this.tradeTaxes = new ArrayList<>(numPeriods);
        this.depoTaxes = new ArrayList<>(numPeriods);
        this.totalMonthlyTaxes = new ArrayList<>(numPeriods);
        sums.forEach(sum -> {
            double tradeTax = taxer.calculateTradeTax(
                    sum == client.getInitialCapital()
                            ? client.getInitialCapital()
                            : client.getMonthlyAddition(),
                    client.getMonthlyDealsNum());
            tradeTaxes.add(tradeTax);
            double rebalancedTax = taxer.calculateTradeTax(sum, client.getMonthlyDealsNum());
            rebalancedTaxes.add(rebalancedTax);
            double depoTax = taxer.calculateDepoTradeTax(sum, client.getMonthlyAddition());
            depoTaxes.add(depoTax);
            double sumTaxes = tradeTax + depoTax;
            totalMonthlyTaxes.add(sumTaxes);
            double rebalancedSumTaxes = rebalancedTax + depoTax;
            this.rebalancedSumTaxes.add(rebalancedSumTaxes);
            totalPeriodRebalancedTaxes +=rebalancedSumTaxes;
            totalPeriodTaxes += sumTaxes;
        });
    }

    List<Integer> getSums() {
        return sums;
    }

    List<Double> getTradeTaxes() {
        return tradeTaxes;
    }

    List<Double> getDepoTaxes() {
        return depoTaxes;
    }

    List<Double> getTotalMonthlyTaxes() {
        return totalMonthlyTaxes;
    }

    double getTotalPeriodTaxes() {
        return totalPeriodTaxes;
    }

    int getNumPeriods() {
        return numPeriods;
    }

    List<Double> getRebalancedTaxes() {
        return rebalancedTaxes;
    }

    List<Double> getRebalancedSumTaxes() {
        return rebalancedSumTaxes;
    }

    double getTotalPeriodRebalancedTaxes() {
        return totalPeriodRebalancedTaxes;
    }
}
