package me.hataProduction.feeCalculator.brokers.tinkoff;

import me.hataProduction.feeCalculator.brokers.Taxer;

public class TinkoffBroker implements Taxer {

    @Override
    public double getTradeTaxByDealsSum(int dealsSum) {
        return 0.3;
    }

    @Override
    public double calculateTradeTax(int monthlyDealsSum, int monthlyDealsNum) {
        return (double) monthlyDealsSum / 100 * getTradeTaxByDealsSum(monthlyDealsSum);
    }

    @Override
    public double calculateDepoTradeTax(int sum, int step) {
        return 99;
    }
}
