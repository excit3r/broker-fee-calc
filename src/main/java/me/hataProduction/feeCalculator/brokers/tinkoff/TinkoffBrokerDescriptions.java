package me.hataProduction.feeCalculator.brokers.tinkoff;

import me.hataProduction.feeCalculator.brokers.HasTaxerDescriptions;

public class TinkoffBrokerDescriptions implements HasTaxerDescriptions {

    @Override
    public String getName() {
        return "Олежка";
    }

    @Override
    public String getTradeTaxDescription() {
        return "0.3% за сумму сделки";
    }

    @Override
    public String getDepoTaxDescription() {
        return "Депозитарий бесплатный, но помимо комиссий берется 99р в мес. за обслуживание чего-то";
    }
}
