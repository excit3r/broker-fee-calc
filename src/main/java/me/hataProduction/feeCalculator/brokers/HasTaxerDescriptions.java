package me.hataProduction.feeCalculator.brokers;

public interface HasTaxerDescriptions {

    String getName();
    String getTradeTaxDescription();
    String getDepoTaxDescription();

}
