package me.hataProduction.feeCalculator.brokers.open;

import me.hataProduction.feeCalculator.brokers.HasTaxerDescriptions;

public class OpenBrokerDescriptions implements HasTaxerDescriptions {

    @Override
    public String getName() {
        return "Открытие";
    }

    @Override
    public String getTradeTaxDescription() {
        return "0.057% + 0.0025% за сделку";
    }

    @Override
    public String getDepoTaxDescription() {
        return "0.00087 или 10р минимум";
    }

}
