package me.hataProduction.feeCalculator.brokers.open;

import me.hataProduction.feeCalculator.brokers.Taxer;

public class OpenBroker implements Taxer {

    @Override
    public double getTradeTaxByDealsSum(int dealsSum) {
        return 0.057;
    }

    @Override
    public double calculateTradeTax(int monthlyDealsSum, int monthlyDealsNum) {
        double tradeTax = getTradeTaxByDealsSum(monthlyDealsSum);
        double transactionTax = 0.0025;
        return ((double) monthlyDealsSum) / 100 * (tradeTax + transactionTax);
    }

    @Override
    public double calculateDepoTradeTax(int sum, int step) {
        double monthly = ((double) sum + (double) step) / 100 * 0.00087;
        return monthly < 10 ? 10 : monthly;
    }
}
