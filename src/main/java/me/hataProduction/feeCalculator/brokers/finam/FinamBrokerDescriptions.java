package me.hataProduction.feeCalculator.brokers.finam;

import me.hataProduction.feeCalculator.brokers.HasTaxerDescriptions;

public class FinamBrokerDescriptions implements HasTaxerDescriptions {

    @Override
    public String getName() {
        return "Финам";
    }

    @Override
    public String getTradeTaxDescription() {
        return "\nдо 1 000 000\t0,0354\n" +
                "свыше 1 000 000 до 5 000 000\t0,0295\n" +
                "свыше 5 000 000 до 10 000 000\t0,0236\n" +
                "свыше 10 000 000 до 20 000 000\t0,0177\n" +
                "свыше 20 000 000 до 50 000 000\t0,01534\n" +
                "свыше 50 000 000 до 100 000 000\t0,0118\n" +
                "свыше 100 000 000\t0,00944 При этом размер комиссионного вознаграждения Брокера за исполнение (частичное исполнение) одного поручения Клиента, составляет не менее 41 руб. 30 коп.";
    }

    @Override
    public String getDepoTaxDescription() {
        return "177р";
    }
}
