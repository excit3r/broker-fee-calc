package me.hataProduction.feeCalculator.brokers.finam;

import me.hataProduction.feeCalculator.brokers.Taxer;

public class FinamBrokerTaxes implements Taxer {

    public double getTradeTaxByDealsSum(int dealsSumAmount) {
        if (dealsSumAmount < 1_000_000) {
            return 0.0354;
        } else if (dealsSumAmount < 5_000_000) {
            return 0.0295;
        } else if (dealsSumAmount < 10_000_000) {
            return 0.0236;
        } else if (dealsSumAmount < 20_000_000) {
            return 0.0177;
        } else if (dealsSumAmount < 50_000_000) {
            return 0.01534;
        } else if (dealsSumAmount < 100_000_000) {
            return 0.0118;
        } else {
            return 0.00944;
        }
    }

    @Override
    public double calculateTradeTax(int monthlyDealsSum, int monthlyDealsNum) {
        double tradeTax = getTradeTaxByDealsSum(monthlyDealsSum);
        double minimumThresholdDealLimit = 41.30;

        double averageDealTaxCost = ((double) monthlyDealsSum / 100) / monthlyDealsNum * tradeTax;
        return averageDealTaxCost > minimumThresholdDealLimit ? averageDealTaxCost * monthlyDealsNum : minimumThresholdDealLimit * monthlyDealsNum;
    }

    @Override
    public double calculateDepoTradeTax(int sum, int step) {
        return 177;
    }
}
