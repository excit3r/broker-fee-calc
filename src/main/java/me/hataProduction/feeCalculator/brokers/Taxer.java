package me.hataProduction.feeCalculator.brokers;

public interface Taxer {
    double getTradeTaxByDealsSum(int dealsSum);
    double calculateTradeTax(int monthlyDealsSum, int monthlyDealsNum);
    double calculateDepoTradeTax(int sum, int step);
}
