package me.hataProduction.feeCalculator.brokers.bks;

import me.hataProduction.feeCalculator.brokers.HasTaxerDescriptions;

public class BksBrokerDescriptions implements HasTaxerDescriptions {

    @Override
    public String getName() {
        return "БКС";
    }

    @Override
    public String getTradeTaxDescription() {
        return "\nДо 100 000 0,0531%\n" +
                "От 100 000 до 300 000 0,0413%\n" +
                "От 300 000 до 1 000 000 0,0354%\n" +
                "От 1 000 000 до 5 000 000 0,0295%\n" +
                "От 5 000 000 до 15 000 000 0,0236%\n" +
                "От 15 000 000 0,0177% + 0.0015% за операции по сделкам, но не менее 177р в мес.";
    }

    @Override
    public String getDepoTaxDescription() {
        return "177р в мес.";
    }

}
