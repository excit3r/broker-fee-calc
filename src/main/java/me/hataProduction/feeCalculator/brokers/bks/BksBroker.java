package me.hataProduction.feeCalculator.brokers.bks;

import me.hataProduction.feeCalculator.brokers.Taxer;

public class BksBroker implements Taxer {

    public double getTradeTaxByDealsSum(int monthlyAmount) {
        if (monthlyAmount < 100_000) {
            return 0.0531;
        } else if (monthlyAmount < 300_000) {
            return 0.0431;
        } else if (monthlyAmount < 1_000_000) {
            return 0.0354;
        } else if (monthlyAmount < 5_000_000) {
            return 0.0295;
        } else if (monthlyAmount < 15_000_000) {
            return 0.0236;
        } else {
            return 0.0177;
        }
    }

    @Override
    public double calculateTradeTax(int monthlyDealsSum, int monthlyDealsNum) {
        double tradeTax = getTradeTaxByDealsSum(monthlyDealsSum);
        double transactionTax = 0.0015;
        double totalTax = ((double) monthlyDealsSum) / 100 * (tradeTax + transactionTax);
        return totalTax < 177 ? 177 : totalTax;
    }

    @Override
    public double calculateDepoTradeTax(int sum, int step) {
        return 177;
    }
}
